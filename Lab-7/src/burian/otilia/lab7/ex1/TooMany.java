package burian.otilia.lab7.ex1;


public class TooMany extends Exception {

    public TooMany(String msg) {
        super(msg);
    }

}