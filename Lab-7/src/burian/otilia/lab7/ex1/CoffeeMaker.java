package burian.otilia.lab7.ex1;

class CoffeeMaker {
    public short count=0;
    Coffee makeCoffee() throws TooMany{
        System.out.println("Make a coffe");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        Coffee cofee = new Coffee(t,c);
        count++;
        if(count>=5){
            throw new TooMany("Too much coffee!!");
        }
        return cofee;
    }

}