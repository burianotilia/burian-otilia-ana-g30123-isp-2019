package burian.otilia.lab2.ex6;


    import java.util.Scanner;

public class FactorialNerecursiv {
    public static void main(String args[]) {
        int i, fact = 1;
        Scanner num = new Scanner(System.in);
        System.out.println("Care este numarul?");
        int N = num.nextInt();
        for (i = 1; i <= N; i++) {
            fact = fact * i;
        }
        System.out.println("Factorialul lui " + N + " este: " + fact);
    }
}