package burian.otilia.lab2.ex6;


import java.util.Scanner;
public class FactorialRecursiv {
    static int factorial(int n){
        if (n == 0)
            return 1;
        else
            return(n * factorial(n-1));
    }
    public static void main(String args[]){
        int i,fact=1;
        Scanner  num = new Scanner(System.in);
        System.out.println("Care este numarul?");
        int N = num.nextInt();
        fact = factorial(N);
        System.out.println("Factorialul lui "+N+" este: "+fact);
    }
}