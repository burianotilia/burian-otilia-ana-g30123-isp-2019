package burian.otilia.lab2.ex4;




        import java.io.*;

        import java.lang.*;

public class MaximVector{

    public static void main(String[] args) throws IOException {

        int n;

        BufferedReader stdin=new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Introduceti numarul de elemente");

        n=Integer.parseInt(stdin.readLine());

        int[] v=new int[n];

        int i;

        for (i=0;i<n;i++)

        {

            System.out.print("V["+i+"]=");

            v[i]=Integer.parseInt(stdin.readLine());

        }

        int max=0;

        for (i=0;i<n;i++)
        {

            if (v[i] > max)

                max = v[i];

        }

        System.out.println("Maximul este  "+max);


    }

}