package burian.otilia.lab6.ex3;

import java.util.Comparator;
import java.util.TreeSet;
import burian.otilia.lab6.ex1.BankAccount;

public class Bank {
    private TreeSet<BankAccount> accounts_owner = new TreeSet<>(Comparator.comparing(BankAccount::getOwner));
    private TreeSet<BankAccount> accounts_balance = new TreeSet<>(Comparator.comparingDouble(BankAccount::getBalance));

    public void addAcount(String owner, double balance) {
        accounts_owner.add(new BankAccount(owner, balance));
        accounts_balance.add(new BankAccount(owner, balance));
    }

    public void printAccounts() {
        for (BankAccount i : accounts_balance)
            System.out.println(i.getOwner() + "  " + i.getBalance());
    }

    public void printAccounts(double minRange, double maxRange) {
        for (BankAccount i : accounts_balance)
            if (i.getBalance() >= minRange && i.getBalance() <= maxRange)
                System.out.println(i.getOwner() + "  " + i.getBalance());
    }

    public void getAllAcounts() {
        for (BankAccount i : accounts_owner)
            System.out.println(i.getOwner() + "  " + i.getBalance());
    }
}