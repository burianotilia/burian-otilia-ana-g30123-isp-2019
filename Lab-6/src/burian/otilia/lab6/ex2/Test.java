package burian.otilia.lab6.ex2;

public class Test {
    public static void main(String[] args) {
        Bank bt = new Bank();
        bt.addAccount("Oana", 50);
        bt.addAccount("Sorina", 200);
        bt.addAccount("Marc", 452);
        bt.addAccount("Ionut", 951);
        bt.addAccount("Dan", 10);
        bt.printAccounts();
        System.out.println("\n Accounts with balance between 300 and 600 RON: \n");
        bt.printAccounts(300,600);
        System.out.println(bt.getAllAccounts());
    }
}
