package burian.otilia.lab6.ex1;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "BankAccount{" + "owner='" + owner + '\'' + ", balance=" + balance + '}';
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    public void withdraw(double amount){
        this.balance=this.balance-amount;
        System.out.println("New balance"+ this.balance);
    }
    public void deposit(double amount){
        this.balance=this.balance+amount;
        System.out.println("New balance"+ this.balance);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==this) {
            return true;
        }

        if(!(obj instanceof BankAccount)){
            return false;
        }

        BankAccount acc=(BankAccount) obj;

        if(owner.equals(acc.owner))
            if(balance==acc.balance)
                return true;
            else
                return false;
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner,balance);

    }
}
