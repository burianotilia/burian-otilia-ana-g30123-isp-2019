package burian.otilia.lab12.ex2;

import java.util.ArrayList;
import java.util.Comparator;


public class Parking {

    /**
     * Vehicles will be parked in parkedVehicles array.
     */
    ArrayList<Vehicle> parkedVehicles=new ArrayList<Vehicle>();

    public void parkVehicle(Vehicle e){
        parkedVehicles.add(e);
    }

    /**
     * Sort vehicles by length.
     */
    public void sortByWeight(){
        parkedVehicles.sort(Comparator.comparing(Vehicle::getWeight));
    }

    public Vehicle get(int index){

        for(int i=0;i<parkedVehicles.size();i++)
            if(i==index)
                return parkedVehicles.get(i);

        return null;
    }

}