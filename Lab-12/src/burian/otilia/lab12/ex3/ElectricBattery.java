package burian.otilia.lab12.ex3;

public class ElectricBattery {

    /**
     * Percentage load.
     */

    private int charge = 0;


    public void charge(){
        try {

            charge++;
            incarcare();
        } catch(BatteryException e){
            System.out.println("Exception "+ e.getMessage());
        }
    }

    public void incarcare () throws BatteryException
    {
        if(charge>100)
            throw new BatteryException("Battery is too high!");

    }

}

