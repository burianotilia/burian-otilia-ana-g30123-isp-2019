package burian.otilia.lab12.ex4;

import java.util.ArrayList;

public class AccountsManager {

    ArrayList<BankAccount> a=new ArrayList<BankAccount>();

    public void addAccount(BankAccount account){
        a.add(account);
    }

    public boolean exists(String id){

        for( BankAccount i : a) {

            if(i.getId()==id)
                return true;

        }

        return false;
    }

    public int count(){

        return a.size();
    }
}
