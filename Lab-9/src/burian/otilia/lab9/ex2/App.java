package burian.otilia.lab9.ex2;

import burian.otilia.lab9.ex1.ButtonAndTextField;
import burian.otilia.lab9.ex1.ButtonAndTextField2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class App extends JFrame {

    int counter=0;
    JLabel nr;
    JLabel da;
    JButton bInc,bDec;


    App() {
        setTitle("Increment");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 250);
        setVisible(true);
    }


    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;

        nr = new JLabel("Numarul");
        nr.setBounds(10, 50, width, height);

        da=new JLabel("da");
        da.setBounds(100, 50, width,height);


        bInc = new JButton("Inc");
        bInc.setBounds(10, 150, width, height);

        bDec = new JButton("Dec");
        bDec.setBounds(100, 150, width, height);

        bInc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                counter++;
                da.setText(""+counter);
            }
        });
        bDec.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                counter--;
                da.setText(""+counter);
            }
        });
        add(bInc);add(da);add(nr);add(bDec);
    }

    public static void main(String[] args) {
        new App();
    }
}
