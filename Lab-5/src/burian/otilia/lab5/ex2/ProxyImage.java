package burian.otilia.lab5.ex2;


import java.util.Scanner;

public class ProxyImage implements Image {

    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;

    private Scanner in = new Scanner(System.in);

    public ProxyImage(String fileName) {
        this.fileName = fileName;
    }

    public void display() {
        System.out.print("Type what image you would like to see (0-real,anything else-rotated): ");
        int x = in.nextInt();
        if (x == 0) {
            if (realImage == null) {
                realImage = new RealImage(fileName);
            }
            realImage.display();
        } else {
            if (rotatedImage == null) {
                rotatedImage = new RotatedImage(fileName);
            }
            rotatedImage.display();
        }
    }

}
