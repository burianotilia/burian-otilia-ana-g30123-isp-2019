package burian.otilia.lab3.ex5;

public class Flower{
    int petal;
    static int numFlowers;
    Flower(){
        numFlowers++;
        System.out.println("Flower has been created!");
    }

    public static int getNumFlowers() {
        return numFlowers;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for(int i =0;i<5;i++){
            Flower f = new Flower();
            garden[i] = f;
        }
    }
}