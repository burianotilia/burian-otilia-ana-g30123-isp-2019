package burian.otilia.lab3.ex2;

public class Circle {
    private double radius=1;
    private String color="red";

    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return Math.PI*Math.pow(radius,2);
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", area='" + getArea() +
                ", color='" + color + '\'' +
                '}';
    }
}

