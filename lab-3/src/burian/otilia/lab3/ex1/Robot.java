package burian.otilia.lab3.ex1;

public class Robot {
    private int x;

    public Robot() {
        this.x = 1;
    }
    public void incx(int k){
        this.x+=k;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                '}';
    }
}