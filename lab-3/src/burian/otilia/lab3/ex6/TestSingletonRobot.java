package burian.otilia.lab3.ex6;


public class TestSingletonRobot {
    public static void main(String[] args) {
        SingletonRobot r1= SingletonRobot.getInstance();
        SingletonRobot r2=SingletonRobot.getInstance();
        System.out.println(r1.toString());
        r1.incx(3);
        System.out.println(r1);
        System.out.println(r2);
    }
}