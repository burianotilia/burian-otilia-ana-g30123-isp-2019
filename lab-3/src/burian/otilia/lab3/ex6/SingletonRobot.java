package burian.otilia.lab3.ex6;

public class SingletonRobot {
    private int x;

    private static SingletonRobot instance = null;

    private SingletonRobot() {
    }

    public static SingletonRobot getInstance(){
        if(instance==null) instance=new SingletonRobot();
        return instance;
    }

    public void incx(int k){
        this.x+=k;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                '}';
    }
}

