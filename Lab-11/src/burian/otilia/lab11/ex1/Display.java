package burian.otilia.lab11.ex1;

import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;

public class Display extends JPanel implements Observer{

    JTextField jtfTemp;
    JLabel jtlTemp;

    Display(){
        this.setLayout(new FlowLayout());
        jtfTemp = new JTextField(20);
        jtlTemp = new JLabel("Temperature");
        add(jtlTemp);add(jtfTemp);
    }

    public void update(Observable o, Object arg) {
        String s = ""+((Sensor)o).getTemperature();
        jtfTemp.setText(s);
    }
}